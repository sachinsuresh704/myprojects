import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router,Link,Route,Switch} from 'react-router-dom';
import Register from './Register'
import Login from './Login'
import Home from './Home'


class App extends Component {

  

  render() {
    return (
      <Router>
      <Switch>
      <Route exact path="/Home" component={Home}/>
        <Route exact path="/Login" component={Login} />
        <Route path="/" component={Register}/>
        
        </Switch>
        </Router>      
    );
  }
}

export default App;

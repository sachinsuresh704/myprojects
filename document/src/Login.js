import React, { Component } from 'react';
import logo from './logo.svg';
import {Form,Button,Col,} from 'react-bootstrap'
import { BrowserRouter as Router,Link,Route,Switch} from 'react-router-dom';
import {Redirect} from 'react-router'
import './App.css';
import axios from 'axios'
import Home from './Home'

class App extends Component {

  constructor(props){
    super(props)
    this.state = {username:'',password:'',redirect:false}
    this.handleChange= this.handleChange.bind(this)
    this.onLogin = this.onLogin.bind(this)

  }


  handleChange(e){
    this.setState({[e.target.name]:event.target.value})
  }

  onLogin(){
    var self= this
    axios.post('/userdatas/loguser',
    {username:this.state.username,
     password:this.state.password})
    .then(function(response){
      this.setState=({redirect:true})
      alert(self.state.userdatas = response.data)
    })
    .catch(function(error){
      console.log(error)
    })
    this.forceUpdate()
  }
  render() {
    if(this.redirect==true){
      return(
        <Redirect to="/Home"/>
      )
    }else{
          return (
      <div align="center">
        <h1>Login</h1>
        <Form >
        <Form.Group as={Col} md="4" controlId="formGroupUsername">
         <Form.Label>Username</Form.Label>
         <Form.Control name="username" value={this.state.username} onChange={e => this.handleChange(e)}  placeholder="Enter Your Username" />
         </Form.Group>
         <Form.Group as={Col} md="4" controlId="formGroupPassword">
         <Form.Label>Password</Form.Label>
         <Form.Control type="password" name="password" value={this.state.password} onChange={e => this.handleChange(e)}  placeholder="Enter Your Password" />
         </Form.Group>
          </Form>
          <Button variant="primary" type="submit" onClick={this.onLogin}>Login</Button>
          <div><Link to="/Home">Updations</Link></div>
      </div>
    );
  }
}
}

export default App;

import React, { Component } from 'react';
import logo from './logo.svg';
import {Form,Button,Table,Col} from 'react-bootstrap'
import './App.css';
import axios from 'axios'

class App extends Component {

    constructor(props){
      super(props)
      this.state ={datas:[],}
      this.getProfile=this.getProfile.bind(this)
      this.deleteOne= this.deleteOne.bind(this)
      this.updateAddress=this.updateAddress.bind(this)
    }

    getProfile()
    {
      var self = this;
      axios.get('/userdatas/getallblogs')
      .then(function (response)
      {
        self.state.blogs = response.data.records
        self.forceUpdate()
      })
        .catch(function (error) {
         console.log(error);
        })
  }
  
    deleteOne(){
      var self= this
      axios.post('/userdatas/deleteone',
      {email:this.state.email})
      .then(function(response){
        self.state.userdatas = response.data
      })
      .catch(function(error){
        console.log(error)
      })
      this.forceUpdate()
    }

    updateAddress(){
      var self= this
      axios.post('/userdatas/updateaddress',
      {address:this.state.address})
      .then(function(response){
        self.state.userdatas = response.data
      })
      .catch(function(error){
        console.log(error)
      })
      this.forceUpdate()
    }

    render() {
        return (
            <div align="center">
            <h1>Profile</h1>
            <Form >
            <Form.Group as={Col} md="4" controlId="formGroupUsername">
             <Form.Label>Username</Form.Label>
             <Form.Control name="username" value={this.state.username} onChange={e => this.handleChange(e)}  placeholder="Enter Your Username" />
             </Form.Group>
             <Form.Group as={Col} md="4" controlId="formGroupPassword">
             <Form.Label>Password</Form.Label>
             <Form.Control type="password" name="password" value={this.state.password} onChange={e => this.handleChange(e)}  placeholder="Enter Your Password" />
             </Form.Group>
              </Form>
              <Button variant="primary" type="submit" onClick={this.getProfile}>Login</Button>
              <Button variant="primary" type="submit" onClick={this.deleteOne}>Login</Button>
              <Button variant="primary" type="submit" onClick={this.updateAddress}>Login</Button>
            
            </div>
        )}

}


import React, { Component } from 'react';
import logo from './logo.svg';
import {Form,Button,Col} from 'react-bootstrap'
import './App.css';
import axios from 'axios'
import { BrowserRouter as Router,Link,Route,Switch} from 'react-router-dom';
import Login from './Login'
class App extends Component {

  constructor(props){
    super(props)
    this.state = {data:[],email:'',username:'',password:''}
    this.saveNewUser= this.saveNewUser.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  
  handleChange(e){
    this.setState({[e.target.name]:event.target.value})
  }

  saveNewUser(){
    var self= this
    axios.post('/userdatas/adduser',
    {email:this.state.email,
     username:this.state.username,
     password:this.state.password,
     address:""})
    .then(function(response){
      self.state.userdatas = response.data
    })
    .catch(function(error){
      console.log(error)
    })
    this.forceUpdate()
  }

  render() {
    return (
      <div align="center">
        <h1>Register</h1>
        <Form >
         <Form.Group as={Col} md="4" controlId="formGroupEmail">
         <Form.Label>Email address</Form.Label>
         <Form.Control  type="email" name="email" value={this.state.email} onChange={e =>this.handleChange(e)}  placeholder="Enter Your email" />
         </Form.Group>
         <Form.Group as={Col} md="4" controlId="formGroupUsername">
         <Form.Label>Username</Form.Label>
         <Form.Control name="username" value={this.state.username} onChange={e => this.handleChange(e)}  placeholder="Enter Your Username" />
         </Form.Group>
         <Form.Group as={Col} md="4" controlId="formGroupPassword">
         <Form.Label>Password</Form.Label>
         <Form.Control type="password" name="password" value={this.state.password} onChange={e => this.handleChange(e)}  placeholder="Enter Your Password" />
         </Form.Group>
          </Form>
      <Button variant="primary" type="submit" onClick={this.saveNewUser}>Register</Button>
      <div> If you are alreday registered<Link to="/Login">Login</Link> </div>   
        </div>
        
    );
  }
}

export default App;

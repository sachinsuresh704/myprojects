var express = require('express')
var router = express.Router()
var multer = require('multer')
var upload = multer({ dest: 'uploads/' })
var userdata = require('../models/userdata')

router.post('/adduser', function(req,res,next){

    var user = new userdata(
        {email:req.body.email,
        username:req.body.username,
        password:req.body.password,
        address:req.body.address})
    user.save(function(err,docs){
        if(err)
            return handleError(err)
            res.send('success')

    })
    
})


router.post('/loguser',function(req,res,next){
    userdata.findOne({username:req.body.username,password:req.body.password}, function(err,docs){
        if (err) {
            res.status(400).send(err);
           } else if (!docs) {
            res.send('must enter details');
           } else {
            res.status(200).send(docs);
           }  })
})

router.post('/deleteone',function(req,res,next){
    userdata.findOneAndDelete({username:req.body.username},{$set:{address:req.body.address}}, function(err,docs){
        if(err){
            res.send('not deleted')
        }else{
            res.send('deleted')
        }
    })
})

router.post('/updateaddress',function(req,res,next){
    userdata.findOneAndUpdate({username:req.body.username},{$set:{address:req.body.address}}, function(err,docs){
        if(err){
            res.send('error in details')
        }else{
            res.send('updated')
        }
    })
})

router.post('/familyupload', upload.single('image') ,function(req, res, next) {
  userdata.findOneAndUpdate({username: req.body.username }, {$set:{familybg: req.file.filename}}, {new: true}, (err, doc) => {
    if(err){
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving notes."
    });
    }else{
      res.send("Update Image Success..!!!")
    }
});
})

module.exports = router
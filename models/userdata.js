var mongoose= require('mongoose')
var Schema = mongoose.Schema
 
var userdataSchema= new Schema({
    email:String,
    username:String,
    password:String,
    address:String,
})
  

module.exports = mongoose.model('Userdata',userdataSchema)